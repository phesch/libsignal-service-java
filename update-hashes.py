#!/usr/bin/env python3
import requests
import hashlib
import json

VERSION = "0.17.0"
TARGETS = [
    "aarch64-unknown-linux-gnu",
    "arm-unknown-linux-gnueabi",
    "arm-unknown-linux-gnueabihf",
    "armv7-unknown-linux-gnueabihf",
    "x86_64-unknown-linux-gnu",
    "x86_64-apple-darwin",
    "x86_64-unknown-linux-musl",
    "aarch64-apple-darwin"
]

urlFormat = "https://gitlab.com/api/v4/groups/6853927/-/packages/maven/org/signald/libsignal-client-{target}/{version}/libsignal-client-{target}-{version}.{ext}"
output = {}

for target in TARGETS:
    output[target] = {}
    for ext in ["jar", "module"]:
        url = urlFormat.format(target=target, version=VERSION, ext=ext)
        print("fetching {}".format(url))
        r = requests.get(url, allow_redirects=True)
        r.raise_for_status()
        output[target][ext] = hashlib.sha256(r.content).hexdigest()

with open('hashes.json', 'w') as f:
    json.dump(output, f, indent=4)
