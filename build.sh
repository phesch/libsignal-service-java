#!/bin/bash
set -exuo pipefail
git clone -b "${version}" https://github.com/Turasa/libsignal-service-java.git
cd libsignal-service-java
git config user.email nobody@signald.org
git config user.name signald
git am ../libsignal-service-java.patch
git am ../update-kbs-settings.patch
python3 ../update-verification-metadata.py
git diff --color=always
gradle assemble publish || (gradle --write-verification-metadata sha256 && git diff --color=always && exit 1)
